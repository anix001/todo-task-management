
export const Navbar = () => {
    return <div className="nav">
       <div className="container">
           <div className="navbar">
               <div className="logo">
                   LoGo
               </div>
               <div className="nav-items">
                   <div className="nav-item">
                       <li>Search</li>
                   </div>
               </div>
           </div>
       </div>
    </div>;
}
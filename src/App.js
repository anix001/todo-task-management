import { Landing } from './Pages/Landing';
import './Scss/App.css';


function App() {
  return (
    <div className="App">
      <Landing/>
    </div>
  );
}

export default App;

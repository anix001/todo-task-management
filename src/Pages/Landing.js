import { useState } from "react";
import { AddTask } from "../Components/AddTask";
import { Dialog } from "../Components/Common/Dialog";
import { EditTask } from "../Components/EditTask";
import { Navbar } from "../Components/Partial/Navbar";
import {TaskContainer} from "../Containers/TaskContainer";
import { TASKS } from "../Utils/mockdata/tasks";


export const Landing = () => {
    const [tasks,setTasks] = useState([...TASKS]);
    const [isAppear , setIsAppear] = useState(false);
    const [isEditAppear,setIsEditAppear] = useState(false);
    const [editTask, setEditTask] = useState({
        id:'',
        title:'',
        description:'',
        tag:'',
        status:'todo'
    });
    const [editItemId, setEditItemId]= useState(null);
    

    const taskFilterHandler = (value) => {
        return tasks.filter(v =>v.status===value)
    }
    
    const toggleAppear = () => {
        setIsAppear(!isAppear);
    }
    const closeTag =() =>{
        setIsEditAppear(!isEditAppear);
    }
    
    //to get the selected task id and values of the task attributes
    const editItem = (id) => {
        setIsEditAppear(!isEditAppear);
        const editid = tasks.find(v =>v.id ===id);
        console.log(editid);
        // setTasks(
        //     tasks.map((item) => { 
        //         if(item.id === id) {
        //             return {...item,title:editid.title, description:editid.description,tag:editid.tag,status:editid.status}   
        //         }  
        //         return item;
        //     })
        // )
        setEditTask({
            id:editid.id,
            title:editid.title,
            description:editid.description,
            tag:editid.tag,
            status:editid.status
        })
        setEditItemId(id);
    }
    
    

     //to add the task to taskcontainer
    const addTask = (task) => {
        setTasks([...tasks,task]);
        toggleAppear();
    }
    
    //to  delete the task from taskcontainer
    const deleteTask = (id) =>{
      const tempTasks = tasks.filter(v =>v.id !==id);
      setTasks([...tempTasks])
    }


    return <>
     <Navbar/>
     <section className="home">
     <div className="section-title">
         Task List
     </div>
     <div className="add-task-btn">
       <button className="btn primary" onClick={toggleAppear}>Add Task</button>
     </div>
     <div className="section-description">
         Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente ullam provident ipsum impedit tempore amet debitis? Natus, rem modi. Dolore laborum minus accusamus aut. 
         Aliquam voluptas dolore ab ipsa ad.
     </div>
     <div className="task-content">
         <TaskContainer title="todo" tasks={taskFilterHandler('todo')} deleteTask={deleteTask} editAppear={editItem}/>
         <TaskContainer title="In Progress" tasks={taskFilterHandler('inProgress')} deleteTask={deleteTask} editAppear={editItem}/>
         <TaskContainer title="Review" tasks={taskFilterHandler('review')} deleteTask={deleteTask} editAppear={editItem}/>
         <TaskContainer title="Complete" tasks={taskFilterHandler('completed')} deleteTask={deleteTask} editAppear={editItem}/>
     </div>
     </section>
     {isAppear && <Dialog closeDialog={toggleAppear}>
         <AddTask closeDialog={toggleAppear} addTask={addTask}/>
     </Dialog>}
     { isEditAppear && <Dialog closeDialog={closeTag}>
         <EditTask closeDialog={closeTag} editContent={editTask} itemId={editItemId}/>
     </Dialog>}
    </>;
}